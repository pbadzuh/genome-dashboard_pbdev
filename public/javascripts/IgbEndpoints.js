let IGBURL = 'http://127.0.0.1:7085/igbStatusCheck';

let igbIsRunning = async () => {
  try {
    console.log(await fetch(IGBURL));
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
}

function checkBrowserSupport(){
    console.log(navigator.userAgent);
    if(navigator.userAgent.indexOf("Chrome")>-1 || navigator.userAgent.indexOf("Firefox")>-1 || navigator.userAgent.indexOf("Edg")>-1){
        document.getElementById("browser_support_bar").innerHTML = "";
        document.getElementById("browser_support_bar").hidden = true;
        document.getElementById("browser_support_bar").remove();
    }
    else{
        document.getElementById("browser_support_bar").innerText = "To open a genome in IGB, use a different Web browser: Firefox, Chrome, or Edge."
    }
}

let igbRedirect = (ev, data) => {
    console.log(navigator.userAgent);
    if(navigator.userAgent.indexOf("Chrome")>-1 || navigator.userAgent.indexOf("Firefox")>-1 || navigator.userAgent.indexOf("Edg")>-1){
        if (ev.target.className == 'material-icons') {
            return
        }
        igbIsRunning().then(running => {
            if (!running) {
                $('#myModal').modal('toggle');
            }
            else {
                window.location = 'http://localhost:7085/IGBControl?version=' + data;
                setTimeout((data) => {
                    window.location = 'http://localhost:7085/bringIGBToFront';
                }, 250);
            }
        })
    }
    else{
        $('#unsupportedBrowserDialog').modal('toggle');
        return
    }
};

